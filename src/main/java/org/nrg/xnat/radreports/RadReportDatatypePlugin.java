/*
 * radreport-datatype-plugin: org.nrg.xnat.radreports.plugin.RadReportDatatypePlugin
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.radreports;

import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;
import org.nrg.xdat.bean.RadRadreportrpt101Bean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
        value = "radReportDatatypePlugin",
        name = "RadReport Datatype Plugin",
        dataModels = {
                @XnatDataModel(
                        value = RadRadreportrpt101Bean.SCHEMA_ELEMENT_NAME,
                        singular = "Bone Age Assessment",
                        plural = "Bone Age Assessment"
                )
        }
)
public class RadReportDatatypePlugin {
    //  [*L*]  //
}
